import * as React from 'react'; // Importa a Biblioteca do React
import axios from 'axios'; // Importa o Axios (Cliente HTTP)
import 'bootstrap/scss/bootstrap.scss'; // Importa o Boostrap (Framework UI)
import './App.scss'; // Importa o Sass do Componente

export interface AppProps { // Interface de Propriedades do  React
}

class App extends React.Component<AppProps, any> { // A classe aqui é só para trazer os planos e renderizar

  constructor(props: any) { // construtor do componente react
    super(props); // passa as propriedades do componente ao react
    this.state = { plans: null, currentType: '' }; // seta o estado inicial do componente
  }

  async getPlans() { // faz a requisição assincrona para trazer os dados do backend
    const res = await axios('http://localhost:3000/list-all-broadband');
    return res;
  }

  handleType = (type: string) => { // seta o tipo de plano no estado
    this.setState({
      currentType: type
    });
  }

  componentDidMount() { // depois que o componente é montado
    if (!this.state.plans) {
      (async () => {
        try {
          this.setState({ plans: await this.getPlans() }); // tenta trazer os planos do backend
        } catch (e) {
          console.error('Error: Load Plans');
        }
      })();
    }
  }

  render() { // renderiza o componente
    return (
      <div className="app">
        <header>
          <div className="container">
            <div className="row">
              <div className="col-12 text-center">
                <a className="logo-link" onClick={() => this.handleType('')} >
                <img src="assets/peppa.jpg" className="logo" alt="logo" />
                </a>
              </div>
            </div>
          </div>
        </header>

        <main>
          <div className="container">
            <div className="row">
              <div className="col-10 offset-1 text-center">
                <p className="hello">Hello little friend, i'm a Peppa, and i'll help you to find a best plan,
                please, choose a plan type</p>
              </div>
            </div>
            <div className="row">
              <div className="col-4 text-center">
                <a className="link" onClick={() => this.handleType('bb')} >
                  <img src="assets/internet.svg" className="internet icon" alt="Internet" />
                  <div className={'title ' + (this.state.currentType === 'bb' ? 'active' : '' )} >Broadband</div>
                </a>
              </div>

              <div className="col-4 text-center">
                <a className="link" onClick={() => this.handleType('ll')} >
                  <img src="assets/phone.svg" className="phone icon" alt="Telefone" />
                  <div className={'title ' + (this.state.currentType === 'll' ? 'active' : '' )}>Landline</div>
                </a>
              </div>

              <div className="col-4 text-center">
                <a className="link" onClick={() => this.handleType('tv')} >
                  <img src="assets/tv.svg" className="tv icon" alt="TV" />
                  <div className={'title ' + (this.state.currentType === 'tv' ? 'active' : '' )}>TV</div>
                </a>
              </div>
            </div>

            {
              this.state.plans ? // se tiver os planos do backend ele exibe de acordo com o tipo selecionado
                <div className="flex-container plans">{
                  this.state.plans.data
                    .filter((item: any) => {  return (item.type === this.state.currentType); })
                    .map((item: any, index: any) => (
                    <div key={index} className="card">
                      <div className="card-body">
                        <p className="card-text">
                          {item.name}
                          <span className="price">$ {item.price}</span>
                        </p>

                      </div>
                    </div>
                  ))}
                </div>
                : this.state.plans
            }

          </div>
        </main>

      </div >
    );
  }
}
export default App;
